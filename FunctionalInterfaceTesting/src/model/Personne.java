package model;

import lombok.Builder;

@Builder
public record Personne(int age, String nom)
{
}
