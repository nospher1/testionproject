package interfaces;

@FunctionalInterface
public interface Diviseur {
        int Diviser(int a, int b);
}
