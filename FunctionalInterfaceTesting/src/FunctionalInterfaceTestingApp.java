import interfaces.Diviseur;
import model.Personne;

import java.util.function.Function;

public class FunctionalInterfaceTestingApp {


    public static void main(String[] args) {

        Diviseur fonctionDeDivision = (a,b) -> b != 0 ? a / b : -1;


        for (int i = 0; i < 10; i++) {
            System.out.println(getResultat(10, i, fonctionDeDivision));
        }

        Function<Integer, String> function = param -> {
            param = param + 10;
            return String.valueOf(param);

        };

        System.out.println(function.apply(50));

    }



    private static int getResultat(int a, int b, Diviseur fonction) {
        return fonction.Diviser(a,b);
    }


    public static String add(String string, Function<String, String> fn) {
        return fn.apply(string);
    }
}
